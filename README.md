# The Rain Virtual Machine
RVM is a fast, secure process virtual machine aimed to be used by languages/compilers and to be cross-platform and type secure.

RVM ensures type safety at runtime by making use of enums and pattern matching, nevertheless using all security advantages of the Rust programming language.

## Installation
### Build with Cargo
RVM works out-of-the-box without any dependency, but if the `full` feature is enabled, it'll make use of `serde` and `bincode` crates to save and load RVM programs on disk.

Basic install (without support to read and write programs):

    cargo build --release

With full features support:

    cargo build --release --features full

Run with:

    cd ./target/release/
    ./rvm [input_file] <arguments>

## Usage
    ./rvm -h

### Examples
Read an assembly source file and execute it:

    ./rvm test.rvm

Read an assembly source file and write bytecode out of it:

    ./rvm test.rvm -o test.out

Load RVM bytecode and disassemble it:

    ./rvm test.out -e -d test.rvm

Load RVM bytecode and disassemble it to stdout:

    ./rvm test.out -e -d

mod rvm;

use rvm::*;
use std::process::exit;

macro_rules! print_usage {
    ($exe:ident) => {
        eprintln!("{} [input_file] <options>", $exe);
        #[cfg(feature = "full")]
        eprintln!("-o [output_file] ");
        #[cfg(feature = "full")]
        eprintln!(
            "-e               Flag whether [input_file] should be treated as a RVM executable"
        );
        eprintln!("-l [integer]     Set max number of instructions to execute");
        eprintln!("-h               Display this help");
    };
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let exe = args[0].as_str();
    if args.len() < 2 {
        print_usage!(exe);
        eprintln!("Too few arguments!");
        exit(1);
    }

    let finput = args[1].as_str();
    #[cfg(feature = "full")]
    let mut foutput: Option<&str> = None;
    let mut disassemble: (bool, Option<&str>) = (false, None);
    let mut inst_limit: Option<usize> = None;
    #[cfg(feature = "full")]
    let mut executable = false;

    let mut i = 2;
    while i < args.len() {
        match args[i].as_str() {
            #[cfg(feature = "full")]
            "-e" => {
                executable = true;
                i += 1;
                continue;
            }
            #[cfg(feature = "full")]
            "-o" => {
                i += 1;
                if args.len() <= i {
                    foutput = Some("rvm.out");
                } else {
                    if args[i].starts_with("-") {
                        continue;
                    }
                    foutput = Some(args[i].as_str());
                }
            }
            "-d" => {
                i += 1;
                if args.len() <= i {
                    disassemble = (true, None);
                } else {
                    if args[i].starts_with("-") {
                        disassemble = (true, None);
                        continue;
                    }
                    disassemble = (true, Some(args[i].as_str()));
                }
            }
            "-l" => {
                i += 1;
                if args.len() <= i || args[i].starts_with("-") {
                    print_usage!(exe);
                    eprintln!("Please specify an instruction limit");
                    exit(1);
                }
                if let Ok(il) = args[i].parse::<usize>() {
                    inst_limit = Some(il);
                } else {
                    print_usage!(exe);
                    eprintln!("Invalid input");
                    exit(1);
                }
            }
            _ => {
                print_usage!(exe);
                eprintln!("Unknown option '{}'", args[i]);
                exit(1);
            }
        }
        i += 1;
    }

    let mut vm = Rvm::init(None);

    #[cfg(feature = "full")]
    if executable {
        if let Err(e) = vm.load_bytecode(finput) {
            eprintln!("Error loading file '{finput}': {e}");
            exit(1);
        }
    } else {
        if let Err(e) = vm.get_program_from_asm(finput) {
            eprintln!("{e}");
            exit(1);
        }
    }

    #[cfg(not(feature = "full"))]
    if let Err(e) = vm.get_program_from_asm(finput) {
        eprintln!("{e}");
        exit(1);
    }

    #[cfg(feature = "full")]
    if let Some(out) = foutput {
        if let Err(e) = vm.save_bytecode(out) {
            eprintln!("Unable to save bytecode to file '{out}': {e}");
            exit(1);
        }
    }

    if disassemble.0 {
        if let Err(e) = vm.disassemble(disassemble.1) {
            eprintln!("Error while trying to disassemble '{finput}': {e}");
            exit(1);
        }
    }

    // for now we will just always execute
    vm.exec(inst_limit);
}

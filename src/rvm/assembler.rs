use crate::rvm::{Inst, InstKind, Program, Word};
use std::{
    error::Error,
    fmt,
    fs::{File, OpenOptions},
    io::{Read, Write},
};

const ID_VALID_CHARACTERS: [char; 63] = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5',
    '6', '7', '8', '9', '0', '_',
];

#[derive(Debug)]
pub struct ParsingErr;

impl Error for ParsingErr {}

impl fmt::Display for ParsingErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error while parsing source file")
    }
}

type ParseResult<T> = Result<T, ParsingErr>;

#[derive(PartialEq)]
struct Label {
    id: String,
    l: usize,
}

impl Label {
    fn new(id: String, l: usize) -> Self {
        Self { id, l }
    }
}

struct StaticStr {
    id: String,
    index: usize,
    value: String,
}

impl StaticStr {
    fn new(id: String, index: usize, value: String) -> Self {
        Self { id, index, value }
    }

    fn max_index(static_strs: &Vec<Self>) -> usize {
        if static_strs.len() == 0 {
            0
        } else {
            let max_str = static_strs.iter().max_by_key(|x| x.index).unwrap();
            max_str.index + max_str.value.len()
        }
    }
}

struct Macro {
    id: String,
    value: i64, // for the moment we'll just support i64 for macros (this may change in the future)
}

impl Macro {
    fn new(id: String, value: i64) -> Self {
        Self { id, value }
    }
}

struct MacroToResolve {
    id: String,
    index: usize,
    line: usize,
}

impl MacroToResolve {
    fn new(id: String, index: usize, line: usize) -> Self {
        Self { id, index, line }
    }
}

struct AsmContext {
    program: Program,
    labels: Vec<Label>,
    labels_to_resolve: Vec<Label>,
    macros: Vec<Macro>,
    macros_to_resolve: Vec<MacroToResolve>,
    static_strs: Vec<StaticStr>,
}

impl AsmContext {
    fn new(
        program: Program,
        labels: Vec<Label>,
        labels_to_resolve: Vec<Label>,
        macros: Vec<Macro>,
        macros_to_resolve: Vec<MacroToResolve>,
        static_strs: Vec<StaticStr>,
    ) -> Self {
        Self {
            program,
            labels,
            labels_to_resolve,
            macros,
            macros_to_resolve,
            static_strs,
        }
    }

    fn default() -> Self {
        AsmContext::new(
            Program::new(),
            Vec::<Label>::new(),
            Vec::<Label>::new(),
            Vec::<Macro>::new(),
            Vec::<MacroToResolve>::new(),
            Vec::<StaticStr>::new(),
        )
    }
}

macro_rules! err_unknown_token {
    ($token:expr, $l:ident) => {
        eprintln!("Error: Unknown token '{}' at line {}", $token, $l + 1);
        return Err(ParsingErr);
    };
}

macro_rules! err_malformed_op {
    ($token:expr, $l:ident) => {
        eprintln!(
            "Error: Malformed operand after '{}' at line {}",
            $token,
            $l + 1
        );
        return Err(ParsingErr);
    };
}

macro_rules! err_malformed_str {
    ($directive:ident, $l:ident) => {
        eprintln!(
            "Error: Malformed string after '{}' at line {}",
            $directive,
            $l + 1
        );
        return Err(ParsingErr);
    };
}

fn is_valid_id(token: &str) -> bool {
    token.len() != 0
        && (token.chars().nth(0).unwrap().is_numeric()
            || !token.chars().all(|c| ID_VALID_CHARACTERS.contains(&c)))
}

fn parse_op(
    inst_kind: InstKind,
    ctx: &mut AsmContext,
    tokens: &Vec<&str>,
    i: &mut usize,
    l: usize,
) -> ParseResult<Option<Word>> {
    if !inst_kind.requires_op() {
        return Ok(None);
    }
    *i += 1;
    if tokens.len() <= 1 {
        eprintln!(
            "Error: Expected operand after '{}' at line {}",
            tokens[*i - 1],
            l + 1
        );
        return Err(ParsingErr);
    }
    use InstKind::*;
    match inst_kind {
        IPush | IAdd | ISub | IMul | IDiv | ICmp => {
            let op = if let Ok(op) = tokens[*i].parse::<i64>() {
                op
            } else {
                if is_valid_id(tokens[*i]) {
                    err_malformed_op!(tokens[*i], l);
                } else {
                    ctx.macros_to_resolve.push(MacroToResolve::new(
                        tokens[*i].to_string(),
                        ctx.program.len(),
                        l,
                    ));
                    0 as i64
                }
            };
            Ok(Some(Word::I64(op)))
        }
        UPush | UAdd | USub | UMul | UDiv | UCmp | ILoad | IStore | ULoad | UStore | FLoad
        | FStore | Jmp | JmpIf | JmpIfG | JmpIfL | JmpIfGe | JmpIfLe | JmpIfEq | Pick | Swap => {
            let op = if let Ok(op) = tokens[*i].parse::<u64>() {
                op
            } else {
                if is_valid_id(tokens[*i]) {
                    err_malformed_op!(tokens[*i], l);
                } else {
                    ctx.macros_to_resolve.push(MacroToResolve::new(
                        tokens[*i].to_string(),
                        ctx.program.len(),
                        l,
                    ));
                    0 as u64
                }
            };
            Ok(Some(Word::U64(op)))
        }
        FPush | FAdd | FSub | FMul | FDiv | FCmp => {
            let op = if let Ok(op) = tokens[*i].parse::<f64>() {
                op
            } else {
                if is_valid_id(tokens[*i]) {
                    err_malformed_op!(tokens[*i], l);
                } else {
                    ctx.macros_to_resolve.push(MacroToResolve::new(
                        tokens[*i].to_string(),
                        ctx.program.len(),
                        l,
                    ));
                    0 as f64
                }
            };
            Ok(Some(Word::F64(op)))
        }
        _ => Ok(None),
    }
}

fn add_label(labels: &mut Vec<Label>, token: &str, l: usize) {
    let token = token.clone();
    let token = token.trim_end_matches(":");
    let label = Label::new(token.to_string(), l);

    if let Some(i) = labels.iter().position(|x| *x == label) {
        labels.remove(i);
    }
    labels.push(label);
}

fn add_macro(macros: &mut Vec<Macro>, id: &str, value: i64) {
    let id = id.clone();
    let macro_ = Macro::new(id.to_string(), value);

    if let Some(i) = macros.iter().position(|x| *x.id == macro_.id) {
        macros.remove(i);
    }
    macros.push(macro_);
}

fn parse_label(labels_to_resolve: &mut Vec<Label>, token: &str, l: usize) {
    // TODO
    let token = token.trim_end_matches(":");
    let label = Label::new(token.to_string(), l);
    labels_to_resolve.push(label);
}

fn resolve_labels(ctx: &mut AsmContext) -> ParseResult<()> {
    for l in &ctx.labels_to_resolve {
        let label = match ctx.labels.iter().find(|x| x.id == l.id) {
            Some(l) => l,
            None => {
                // if it's not a label, it must be a macro
                ctx.macros_to_resolve
                    .push(MacroToResolve::new(l.id.clone(), l.l, 0));
                ctx.program[l.l].op = Word::U64(0);
                return Ok(());
            }
        };
        ctx.program[l.l].op = Word::U64(label.l as u64);
    }

    Ok(())
}

fn resolve_macros(ctx: &mut AsmContext) -> ParseResult<()> {
    for m in &ctx.macros_to_resolve {
        let mb: Macro;
        let macro_ = match ctx.macros.iter().find(|x| x.id == m.id) {
            Some(m) => m,
            None => match ctx.static_strs.iter().find(|x| x.id == m.id) {
                Some(s) => {
                    mb = Macro::new(s.id.clone(), s.index as i64);
                    &mb
                }
                None => {
                    eprintln!("Error: Unknown token '{}' at line {}", m.id, m.line + 1);
                    return Err(ParsingErr);
                }
            },
        };
        let inst = &mut ctx.program[m.index];
        if !inst.kind.requires_op() {
            let l = m.line;
            err_unknown_token!(m.id, l);
        }
        inst.op = match inst.op {
            Word::U64(_) => Word::U64(macro_.value as u64),
            Word::I64(_) => Word::I64(macro_.value as i64),
            Word::F64(_) => Word::F64(macro_.value as f64),
            Word::Default(_) => unreachable!(),
        }
    }
    Ok(())
}

fn resolve_static_strs(ctx: &mut AsmContext, static_mem: &mut [u8]) -> ParseResult<()> {
    for s in &ctx.static_strs {
        let a = s.index;
        let b = s.value.len();
        if static_mem.len() <= a + b {
            eprintln!(
                "Error: Static string at index {} exceeds static memory size of {}",
                a, b
            );
            return Err(ParsingErr);
        }
        static_mem[a..a + b].copy_from_slice(s.value.as_bytes());
    }
    Ok(())
}

fn _get_program_from_asm(asm: &String, ctx: Option<AsmContext>) -> ParseResult<AsmContext> {
    let mut ctx = match ctx {
        Some(c) => c,
        None => {
            let mut ctx = AsmContext::default();
            ctx.program.push(Inst::new(InstKind::Jmp, None));
            ctx
        }
    };

    let lines: Vec<String> = asm.lines().map(|l| l.trim().to_string()).collect();
    'line: for l in 0..lines.len() {
        let line = &lines[l];
        if line.is_empty() || line == "\n" || line == "\r\n" {
            continue;
        }

        // Pass 1: Preprocessor directives
        if line.starts_with("#") {
            // we found a preprocessor directive
            let tokens: Vec<&str> = line.split(" ").filter(|t| !t.is_empty()).collect();
            println!("{tokens:?}");
            let directive = tokens[0].trim_start_matches("#").trim();
            match directive {
                "define" => {
                    if tokens.len() < 2 {
                        eprintln!(
                            "Error: Expected identifier after '{directive}' at line {}",
                            l + 1
                        );
                        return Err(ParsingErr);
                    } else if tokens.len() < 3 {
                        eprintln!("Error: Expected number literal after identifier '{directive}' at line {}", l+1);
                        return Err(ParsingErr);
                    }
                    let id = tokens[1].trim();
                    if is_valid_id(id) {
                        eprintln!("Error: Invalid identifier '{id}' for directive '{directive}' at line {}", l+1);
                        return Err(ParsingErr);
                    }
                    let literal = tokens[2].trim();
                    if literal.starts_with("\"") {
                        let mut literal = String::new();
                        for token in &tokens[2..] {
                            literal.push_str(token.trim());
                            literal.push(' ');
                        }
                        let literal = literal.trim_end_matches(" ");
                        if !literal.ends_with("\"") {
                            let s = "macro declaration";
                            err_malformed_str!(s, l);
                        }
                        let value = &literal[1..literal.len() - 1];
                        ctx.static_strs.push(StaticStr::new(
                            id.to_string(),
                            StaticStr::max_index(&ctx.static_strs),
                            value.to_string(),
                        ));
                        continue 'line;
                    }
                    let value = match literal.parse::<i64>() {
                        Ok(v) => v,
                        Err(_) => {
                            eprintln!("Error: Invalid number literal for '{id}' at line {}", l + 1);
                            return Err(ParsingErr);
                        }
                    };
                    add_macro(&mut ctx.macros, id, value);
                }
                "include" => {
                    if tokens.len() < 2 {
                        eprintln!(
                            "Error: Expected RVM source file after '{directive}' at line {}",
                            l + 1
                        );
                        return Err(ParsingErr);
                    }
                    let fpath = tokens[1].trim();
                    if !fpath.starts_with("\"") {
                        eprintln!(
                            "Error: Unterminated string literal after '{directive}' at line {}",
                            l + 1
                        );
                        return Err(ParsingErr);
                    }
                    if !fpath.ends_with("\"") {
                        err_malformed_str!(directive, l);
                    }
                    let fpath = fpath.trim_start_matches("\"").trim_end_matches("\"");
                    if !fpath.ends_with(".rvm") {
                        eprintln!("Error: Expected RVM source file (.rvm), got '{fpath}' after '{directive}' at line {}", l+1);
                        return Err(ParsingErr);
                    }
                    let mut f = match OpenOptions::new().read(true).open(fpath) {
                        Ok(f) => f,
                        Err(e) => {
                            eprintln!("Error: Unable to load file '{fpath}': {e}");
                            return Err(ParsingErr);
                        }
                    };
                    let mut included_asm = String::new();
                    if let Err(e) = f.read_to_string(&mut included_asm) {
                        eprintln!("Error: Unable to read file '{fpath}': {e}");
                        return Err(ParsingErr);
                    }
                    ctx = _get_program_from_asm(&included_asm, Some(ctx))?;
                }
                _ => {
                    eprintln!(
                        "Error: Unrecognized preprocessor directive '{directive}' at line {}",
                        l + 1
                    );
                    return Err(ParsingErr);
                }
            }
            continue 'line;
        }

        // Pass 2: Instruction parsing
        let tokens: Vec<&str> = line.split(" ").filter(|t| !t.is_empty()).collect();
        let mut i: usize = 0;
        while i < tokens.len() {
            if tokens[i].is_empty() {
                i += 1;
                continue;
            }
            if tokens[i].starts_with(";") {
                continue 'line;
            }
            if tokens[i].ends_with(":") {
                add_label(&mut ctx.labels, &tokens[i], ctx.program.len());
                continue 'line;
            }
            let kind = match InstKind::iterator().find(|k| k.get_token_str().eq(&tokens[i])) {
                Some(k) => k,
                None => {
                    err_unknown_token!(tokens[i], l);
                }
            };
            if kind.requires_label() {
                if !is_valid_id(tokens[i + 1]) {
                    // we found a jmp <label> or a call <label>
                    i += 1;
                    parse_label(&mut ctx.labels_to_resolve, &tokens[i], ctx.program.len());
                    ctx.program.push(Inst::new(kind, Some(Word::U64(0))));
                    continue 'line;
                }
            }
            // 2.1: Operand parsing
            let op = parse_op(kind, &mut ctx, &tokens, &mut i, l)?;
            ctx.program.push(Inst::new(kind, op));
            i += 1;
        }
    }
    Ok(ctx)
}

// wrapper interface to _get_program_from_asm
pub fn get_program_from_asm(asm: &String, static_mem: &mut [u8]) -> ParseResult<Program> {
    let mut ctx = _get_program_from_asm(asm, None)?;
    resolve_static_strs(&mut ctx, static_mem)?;
    resolve_labels(&mut ctx)?;
    resolve_macros(&mut ctx)?;
    if let Some(l) = ctx.labels.iter().find(|l| l.id == "main") {
        ctx.program[0].op = Word::U64(l.l as u64);
    } else {
        ctx.program.remove(0);
    }
    Ok(ctx.program)
}

pub fn disassemble(
    f: Option<&mut File>,
    program: &Program,
    uppercase: bool,
) -> Result<(), Box<dyn Error>> {
    let mut out = String::new();
    for i in 0..program.len() {
        let kind = program[i].kind;
        let inst = kind.get_token_str();
        out.push_str(inst.as_str());
        if kind.requires_op() {
            out.push(' ');
            out.push_str(program[i].op.to_string().as_str());
        }
        out.push('\n');
    }
    if !uppercase {
        out = out.to_lowercase();
    }
    if let Some(f) = f {
        f.write_all(out.as_bytes())?;
    } else {
        println!("{out}");
    }
    Ok(())
}

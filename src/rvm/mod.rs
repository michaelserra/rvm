mod assembler;

#[cfg(feature = "full")]
use serde::{Deserialize, Serialize};
#[cfg(not(feature = "full"))]
use std::io::Read;
#[cfg(feature = "full")]
use std::io::{Read, Write};

use std::error::Error;
use std::fmt;
use std::fs::OpenOptions;

// type DefaultWord = u64;
pub type Program = Vec<Inst>;

const RVM_STACK_SIZE: usize = 2048;
const RVM_STATIC_MEM_SIZE: usize = 1024 * 64; // 64 KB
                                              // const RVM_STATIC_MEM_SIZE: usize = 1024;

#[derive(Debug)]
pub enum Exception {
    StackOverflow,
    StackUnderflow,
    TypeError,
    // InvalidInst,
    InvalidAddr,
    InvalidOp,
    IllegalMemAccess,
}

impl std::error::Error for Exception {}

impl fmt::Display for Exception {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Exception::StackOverflow => {
                write!(f, "[ERROR] StackOverflow: Stack has been overflowed")
            }
            Exception::StackUnderflow => write!(
                f,
                "[ERROR] StackUnderflow: Arguments provided on stack aren't sufficient"
            ),
            Exception::TypeError => write!(
                f,
                "[ERROR] TypeError: Arguments on stack have different type than instruction"
            ),
            // Exception::InvalidInst => write!(f, "[ERROR] InvalidInst: Invalid instruction"),
            Exception::InvalidAddr => write!(f, "[ERROR] InvalidAddr: Invalid address provided"),
            Exception::InvalidOp => write!(f, "[ERROR] InvalidOp: Invalid operand provided"),
            Exception::IllegalMemAccess => {
                write!(f, "[ERROR] IllegalMemAccess: Illegal memory access")
            }
        }
    }
}

type RvmResult<T> = Result<T, Exception>;

#[cfg_attr(feature = "full", derive(Copy, Clone, Debug, Serialize, Deserialize))]
#[cfg_attr(not(feature = "full"), derive(Copy, Clone, Debug))]
pub enum Word {
    Default(i64),
    U64(u64),
    I64(i64),
    F64(f64),
}

impl Word {
    fn default(&self) -> RvmResult<i64> {
        match self {
            Word::Default(ret) => Ok(*ret),
            _ => Err(Exception::TypeError),
        }
    }

    fn u64(&self) -> RvmResult<u64> {
        match self {
            Word::U64(ret) => Ok(*ret),
            _ => Err(Exception::TypeError),
        }
    }

    fn i64(&self) -> RvmResult<i64> {
        match self {
            Word::I64(ret) => Ok(*ret),
            _ => Err(Exception::TypeError),
        }
    }

    fn f64(&self) -> RvmResult<f64> {
        match self {
            Word::F64(ret) => Ok(*ret),
            _ => Err(Exception::TypeError),
        }
    }
}

impl fmt::Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Word::Default(v) => write!(f, "{v}"),
            Word::I64(v) => write!(f, "{v}"),
            Word::F64(v) => write!(f, "{v}"),
            Word::U64(v) => write!(f, "{v}"),
        }
    }
}

#[cfg_attr(
    feature = "full",
    derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)
)]
#[cfg_attr(not(feature = "full"), derive(Copy, Clone, Debug, PartialEq))]
pub enum InstKind {
    IPush,
    IAdd,
    ISub,
    IMul,
    IDiv,
    ICmp,
    ILoad,
    IStore,

    UPush,
    UAdd,
    USub,
    UMul,
    UDiv,
    UCmp,
    ULoad,
    UStore,

    FPush,
    FAdd,
    FSub,
    FMul,
    FDiv,
    FCmp,
    FLoad,
    FStore,

    Return,
    Pop,
    Halt,
    Call,
    Jmp,
    JmpIf,
    JmpIfL,
    JmpIfG,
    JmpIfLe,
    JmpIfGe,
    JmpIfEq,
    Pick,
    Swap,
    And,
    Or,
    Xor,
    Not,
    RShift,
    LShift,
}

impl fmt::Display for InstKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl InstKind {
    pub fn iterator() -> impl Iterator<Item = InstKind> {
        use self::InstKind::*;
        [
            IPush, UPush, FPush, Pop, Halt, IAdd, ISub, IMul, IDiv, UAdd, USub, UMul, UDiv, FAdd,
            FSub, FMul, FDiv, ICmp, UCmp, FCmp, ILoad, IStore, ULoad, UStore, FLoad, FStore, Call,
            Jmp, JmpIf, JmpIfG, JmpIfL, JmpIfGe, JmpIfLe, JmpIfEq, Pick, Swap, Return, And, Or,
            Xor, Not, RShift, LShift,
        ]
        .iter()
        .copied()
    }

    fn requires_op(&self) -> bool {
        use self::InstKind::*;
        match self {
            IPush | UPush | FPush | Call | Jmp | JmpIf | JmpIfG | JmpIfL | JmpIfGe | JmpIfLe
            | JmpIfEq | Swap | Pick | ILoad | IStore | ULoad | UStore | FLoad | FStore => true,
            _ => false,
        }
    }

    fn requires_label(&self) -> bool {
        use self::InstKind::*;
        match self {
            Jmp | JmpIf | JmpIfG | JmpIfL | JmpIfGe | JmpIfLe | JmpIfEq | Call => true,
            _ => false,
        }
    }

    fn get_token_str(&self) -> String {
        match self {
            InstKind::JmpIfL => "jlt".to_string(),
            InstKind::JmpIfG => "jgt".to_string(),
            InstKind::JmpIfLe => "jle".to_string(),
            InstKind::JmpIfGe => "jge".to_string(),
            InstKind::JmpIfEq => "jeq".to_string(),
            InstKind::RShift => "rsh".to_string(),
            InstKind::LShift => "lsh".to_string(),
            _ => self.to_string().to_lowercase(),
        }
    }
}

#[cfg_attr(feature = "full", derive(Copy, Clone, Debug, Serialize, Deserialize))]
#[cfg_attr(not(feature = "full"), derive(Copy, Clone, Debug))]
pub struct Inst {
    kind: InstKind,
    op: Word,
}

impl Inst {
    pub fn new(kind: InstKind, op: Option<Word>) -> Self {
        Self {
            kind,
            op: op.unwrap_or(Word::Default(0)),
        }
    }
}

pub struct Rvm {
    stack: [Word; RVM_STACK_SIZE],
    static_mem: [u8; RVM_STATIC_MEM_SIZE],
    sp: usize,
    pc: usize,
    program: Program,
    halt: bool,
}

impl Rvm {
    pub fn init(program: Option<Program>) -> Self {
        Self {
            stack: [Word::Default(0); RVM_STACK_SIZE],
            static_mem: [0; RVM_STATIC_MEM_SIZE],
            sp: 0,
            pc: 0,
            halt: false,
            program: program.unwrap_or(Program::new()),
        }
    }

    fn inc_pc(&mut self) {
        self.pc += 1;
    }

    fn dec_sp(&mut self) {
        self.sp -= 1;
    }

    fn inc_sp(&mut self) {
        self.sp += 1;
    }

    fn check_overflow(&self) -> RvmResult<()> {
        if self.sp >= RVM_STACK_SIZE {
            return Err(Exception::StackOverflow);
        }
        Ok(())
    }

    fn check_underflow(&self, n: usize) -> RvmResult<()> {
        if self.sp < n {
            return Err(Exception::StackUnderflow);
        }
        Ok(())
    }

    fn push(&mut self, value: Word) -> RvmResult<()> {
        self.check_overflow()?;
        self.stack[self.sp] = value;
        self.inc_sp();
        Ok(())
    }

    pub fn load_program(&mut self, program: Program) {
        self.program = program.clone();
    }

    fn dump_stack(&self) {
        for i in 0..self.sp {
            print!("{} ", self.stack[i]);
        }
        println!();
    }

    fn dump_static_mem(&self) {
        for i in 0..self.static_mem.len() {
            print!("{} ", self.static_mem[i] as char);
        }
        println!();
    }

    fn exec_inst(&mut self, inst: Inst) -> RvmResult<()> {
        macro_rules! arithmetic_op {
            ($self:ident, $F:ident, $FW:ident, $X:tt) => {
                self.check_underflow(2)?;
                let op1 = $self.stack[$self.sp - 2].$F()?;
                let op2 = $self.stack[$self.sp - 1].$F()?;
                $self.stack[$self.sp - 2] = Word::$FW(op1 $X op2);
                //$self.dec_sp();
                $self.dec_sp();
                $self.inc_pc();
            };
        }

        macro_rules! op_u64 {
            () => {
                match inst.op.u64() {
                    Ok(op) => op,
                    Err(_) => return Err(Exception::InvalidOp),
                }
            };
        }

        macro_rules! arithmetic_cmp {
            ($self:ident, $F:ident) => {
                $self.check_underflow(2)?;
                let op1 = $self.stack[$self.sp - 2].$F()?;
                let op2 = $self.stack[$self.sp - 1].$F()?;
                $self.dec_sp();
                $self.dec_sp();
                if op1 > op2 {
                    $self.push(Word::Default(1))?;
                } else if op1 < op2 {
                    $self.push(Word::Default(-1))?;
                } else {
                    $self.push(Word::Default(0))?;
                }
                self.inc_pc();
            };
        }

        macro_rules! jump_if {
            ($X:tt) => {
                self.check_overflow()?;
                let op = op_u64!() as usize;
                if op >= self.program.len() {
                    return Err(Exception::InvalidAddr);
                }
                let flag = self.stack[self.sp - 1].default()?;
                if flag $X 0 {
                    self.pc = op;
                } else {
                    self.inc_pc();
                }
                self.dec_sp();
            }
        }

        macro_rules! push {
            ($self:ident, $F:ident) => {
                if let Word::$F(_) = inst.op {
                    $self.push(inst.op)?;
                } else {
                    return Err(Exception::TypeError);
                }
                $self.inc_pc();
            };
        }

        macro_rules! bitwise_op {
            ($self:ident, $X:tt) => {
                $self.check_underflow(2)?;
                let a = $self.stack[$self.sp - 2];
                let b = $self.stack[$self.sp - 1];
                let y: Word = match (a, b) {
                    (Word::U64(a), Word::U64(b)) => Word::U64(a $X b),
                    (Word::I64(a), Word::I64(b)) => Word::I64(a $X b),
                    (Word::F64(a), Word::F64(b)) => {
                        // in this case bitwise operation will be performed roughly as bits, since
                        // it is not guaranteed how floating point numbers are stored on different
                        // machines (i.e. if they respect IEEE 754)
                        Word::F64(f64::from_bits(f64::to_bits(a) $X f64::to_bits(b)))
                    }
                    (Word::Default(a), Word::Default(b)) => Word::Default(a $X b),
                    _ => return Err(Exception::TypeError),
                };
                $self.dec_sp();
                $self.dec_sp();
                $self.push(y)?;
                $self.inc_pc();
            };
        }

        macro_rules! static_load {
            ($self:ident, $type:tt, $F:ident) => {
                $self.check_underflow(1)?;
                let addr = $self.stack[$self.sp - 1].u64()? as usize;
                $self.dec_sp();
                let bytes = op_u64!() as usize;
                if bytes % 2 != 0 || bytes > 8 || bytes < 1 {
                    // atm we'll just support maximum 64-bit
                    if bytes != 1 {
                        return Err(Exception::InvalidOp);
                    }
                }
                if addr + bytes >= RVM_STATIC_MEM_SIZE {
                    return Err(Exception::IllegalMemAccess);
                }
                if bytes == 1 {
                    let ret = $self.static_mem[addr] as $type;
                    $self.push(Word::$F(ret as $type))?;
                } else {
                    let n_slice = &$self.static_mem[addr..addr + bytes];
                    let mut n: [u8; 8] = [0; 8];
                    n[..bytes].copy_from_slice(&n_slice);
                    if cfg!(target_endian = "big") {
                        $self.push(Word::$F($type::from_be_bytes(n)))?;
                    } else {
                        $self.push(Word::$F($type::from_le_bytes(n)))?;
                    }
                };
                $self.inc_pc();
            };
        }

        macro_rules! static_store {
            ($self:ident, $type:tt, $F:ident) => {
                $self.check_underflow(2)?;
                let addr = $self.stack[$self.sp - 1].u64()? as usize;
                $self.dec_sp();
                let op = $self.stack[$self.sp - 1].$F()?;
                let bytes = op_u64!() as usize;
                if bytes % 2 != 0 || bytes > 8 || bytes < 1 {
                    if bytes != 1 {
                        return Err(Exception::InvalidOp);
                    }
                }
                if addr + bytes >= RVM_STATIC_MEM_SIZE {
                    return Err(Exception::IllegalMemAccess);
                }
                if bytes == 1 {
                    $self.static_mem[addr] = op as u8;
                } else {
                    let data = if cfg!(target_endian = "big") {
                        op.to_be_bytes()
                    } else {
                        op.to_le_bytes()
                    };
                    let data = &data[0..bytes];
                    $self.static_mem[addr..addr + bytes].copy_from_slice(&data);
                }
                $self.inc_pc();
            };
        }

        use InstKind::*;
        match inst.kind {
            Halt => self.halt = true,
            Return => {
                self.check_underflow(2)?;

                // swap last two values
                let a = self.sp - 1;
                let b = self.sp - 2;
                let t = self.stack[a];
                self.stack[a] = self.stack[b];
                self.stack[b] = t;

                let op = self.stack[self.sp - 1].u64()? as usize;
                if op >= self.program.len() {
                    return Err(Exception::InvalidAddr);
                }
                self.pc = op;
                self.dec_sp();
                self.inc_pc();
            }
            Call => {
                let op = op_u64!() as usize;
                if op >= self.program.len() {
                    return Err(Exception::InvalidAddr);
                }
                self.push(Word::U64(self.pc as u64))?;
                self.pc = op;
            }
            IPush => {
                push!(self, I64);
            }
            UPush => {
                push!(self, U64);
            }
            FPush => {
                push!(self, F64);
            }
            Pop => {
                self.check_underflow(1)?;
                self.dec_sp();
                self.inc_pc();
            }
            IAdd => {
                arithmetic_op!(self, i64, I64, +);
            }
            ISub => {
                arithmetic_op!(self, i64, I64, -);
            }
            IMul => {
                arithmetic_op!(self, i64, I64, *);
            }
            IDiv => {
                arithmetic_op!(self, i64, I64, /);
            }
            UAdd => {
                arithmetic_op!(self, u64, U64, +);
            }
            USub => {
                arithmetic_op!(self, u64, U64, -);
            }
            UMul => {
                arithmetic_op!(self, u64, U64, *);
            }
            UDiv => {
                arithmetic_op!(self, u64, U64, /);
            }
            FAdd => {
                arithmetic_op!(self, f64, F64, +);
            }
            FSub => {
                arithmetic_op!(self, f64, F64, -);
            }
            FMul => {
                arithmetic_op!(self, f64, F64, *);
            }
            FDiv => {
                arithmetic_op!(self, f64, F64, /);
            }
            ICmp => {
                arithmetic_cmp!(self, i64);
            }
            UCmp => {
                arithmetic_cmp!(self, u64);
            }
            FCmp => {
                arithmetic_cmp!(self, f64);
            }
            Jmp => {
                let op = op_u64!();
                if op as usize >= self.program.len() {
                    return Err(Exception::InvalidAddr);
                }
                self.pc = op as usize;
            }
            JmpIf => {
                let op = op_u64!();
                if op as usize >= self.program.len() {
                    return Err(Exception::InvalidAddr);
                }
                let flag = self.stack[self.sp - 1].default()?;
                if flag != 0 {
                    self.pc = op as usize;
                } else {
                    self.inc_pc();
                }
                self.dec_sp();
            }
            JmpIfG => {
                jump_if!(>);
            }
            JmpIfL => {
                jump_if!(<);
            }
            JmpIfGe => {
                jump_if!(>=);
            }
            JmpIfLe => {
                jump_if!(<=);
            }
            JmpIfEq => {
                jump_if!(==);
            }
            Pick => {
                self.check_overflow()?;
                let op = op_u64!() as usize;
                if self.sp - op <= 0 {
                    return Err(Exception::StackUnderflow);
                }
                let pick = self.stack[self.sp - 1 - op];
                self.push(pick)?;
                self.inc_pc();
            }
            Swap => {
                let op = op_u64!() as usize;
                self.check_underflow(op)?;
                let a = self.sp - 1;
                let b = self.sp - 1 - op;

                let t = self.stack[a];
                self.stack[a] = self.stack[b];
                self.stack[b] = t;
                self.inc_pc();
            }
            ILoad => {
                static_load!(self, i64, I64);
            }
            ULoad => {
                static_load!(self, u64, U64);
            }
            FLoad => {
                static_load!(self, f64, F64);
            }
            IStore => {
                static_store!(self, i64, i64);
            }
            UStore => {
                static_store!(self, u64, u64);
            }
            FStore => {
                static_store!(self, f64, f64);
            }
            And => {
                bitwise_op!(self, &);
            }
            Or => {
                bitwise_op!(self, |);
            }
            Xor => {
                bitwise_op!(self, ^);
            }
            RShift => {
                bitwise_op!(self, >>);
            }
            LShift => {
                bitwise_op!(self, <<);
            }
            Not => {
                self.check_underflow(2)?;
                let a = self.stack[self.sp - 1];
                let y: Word = match a {
                    Word::U64(a) => Word::U64(!a),
                    Word::I64(a) => Word::I64(!a),
                    Word::F64(a) => Word::F64(f64::from_bits(!f64::to_bits(a))),
                    Word::Default(a) => Word::Default(!a),
                };
                self.dec_sp();
                self.dec_sp();
                self.push(y)?;
                self.inc_pc();
            }
        }
        Ok(())
    }

    pub fn get_program_from_asm(&mut self, fpath: &str) -> Result<(), Box<dyn std::error::Error>> {
        let mut f = OpenOptions::new().read(true).open(fpath)?;
        let mut asm = String::new();
        f.read_to_string(&mut asm)?;
        match assembler::get_program_from_asm(&asm, &mut self.static_mem) {
            Ok(program) => self.load_program(program),
            Err(e) => {
                return Err(Box::from(e));
            }
        }
        Ok(())
    }

    pub fn disassemble(&self, fpath: Option<&str>) -> Result<(), Box<dyn Error>> {
        if let Some(fpath) = fpath {
            let mut f = OpenOptions::new().write(true).create(true).open(fpath)?;
            assembler::disassemble(Some(&mut f), &self.program, true)?;
        } else {
            assembler::disassemble(None, &self.program, true)?;
        }
        Ok(())
    }

    #[cfg(feature = "full")]
    pub fn save_bytecode(&self, fpath: &str) -> Result<(), Box<dyn Error>> {
        let mut f = OpenOptions::new().write(true).create(true).open(fpath)?;
        let to_write = bincode::serialize(&self.program)?;
        f.write_all(&to_write.as_ref())?;
        Ok(())
    }

    #[cfg(feature = "full")]
    pub fn load_bytecode(&mut self, fpath: &str) -> Result<(), Box<dyn Error>> {
        let mut f = OpenOptions::new().read(true).open(fpath)?;
        let mut buffer = Vec::<u8>::new();
        f.read_to_end(&mut buffer)?;
        let to_read: Program = bincode::deserialize(&buffer)?;
        self.load_program(to_read);
        Ok(())
    }

    pub fn exec(&mut self, limit: Option<usize>) {
        let mut i: usize = 0;
        let is_limit = limit.is_some();
        while !self.halt {
            if is_limit && limit.unwrap() == i {
                println!("[INFO] Reached execution limit of {i}");
                return;
            }
            let inst = self.program[self.pc];
            if let Err(e) = self.exec_inst(inst) {
                eprintln!("{e}");
                return;
            }
            println!("{}({})", inst.kind, inst.op);
            self.dump_stack();
            // self.dump_static_mem();
            i += 1;
        }
    }
}
